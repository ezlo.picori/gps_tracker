#!/usr/bin/env bash

# Stop on error
set -e

# ===========================================
# Install current project in development mode
# ===========================================

pip install -e .[dev]

# ====================
# Configure pre-commit
# ====================

pre-commit install \
  --hook-type commit-msg \
  --hook-type pre-commit \
  --hook-type pre-push
